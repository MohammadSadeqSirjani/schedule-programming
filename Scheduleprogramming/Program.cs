﻿using Schedule.Services;
using System;

namespace Scheduleprogramming
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // For Interval in Seconds 
            // This Scheduler will start at 21:50 and call after every 5 Seconds
            // IntervalInSeconds(start_hour, start_minute, seconds)
            TaskScheduler.IntervalInSeconds(22, 11, 5,
            () => {

                Console.WriteLine("//here write the code that you want to schedule");
            });

            // For Interval in Minutes 
            // This Scheduler will start at 21:50 and call after every 1 Minutes
            // IntervalInSeconds(start_hour, start_minute, minutes)
            TaskScheduler.IntervalInMinutes(21, 50, 1,
            () => {

                Console.WriteLine("//here write the code that you want to schedule");
            });

            // For Interval in Hours 
            // This Scheduler will start at 21:50 and call after every 1 Hour
            // IntervalInSeconds(start_hour, start_minute, hours)
            TaskScheduler.IntervalInHours(21, 50, 1,
            () => {

                Console.WriteLine("//here write the code that you want to schedule");
            });

            // For Interval in Seconds 
            // This Scheduler will start at 21:50 and call after every 1 Days
            // IntervalInSeconds(start_hour, start_minute, days)
            TaskScheduler.IntervalInDays(21, 50, 1,
            () => {

                Console.WriteLine("//here write the code that you want to schedule");
            });

            Console.ReadLine();
        }
    }
}
